# CakePHP Develop

Efetuar a instalação do docker conforme o manual em https://docs.docker.com/installation/ubuntulinux/#installing-docker-on-ubuntu.

Dê permissão para o seu usuário poder usar o Docker:

    sudo usermod -aG docker $USER

Clonar este repositório e na pasta do projeto executar:

    docker build -t adrianoluis/cakephp-develop .

Em seguida configure 1 docker para cada projeto que deseja utilizar:

    docker run -p 8050:80 \
        -v $HOME/Projects/cakephp/app:/var/www/html/app \
        -v $HOME/Projects/cakephp/app/tmp/logs:/var/log/nginx \
        -v $HOME/Projects/cakephp/lib:/var/www/html/lib \
        -v $HOME/Projects/cakephp/plugins:/var/www/html/plugins \
        -v $HOME/Projects/cakephp/vendors:/var/www/html/vendors \
        --name cakephp \
        adrianoluis/cakephp-develop &

Ao executar o comando acima, o shell vai ficar travado por conta do script RUN do Docker. Isso só ocorre no momento de criação do Docker. Depois de criado basta iniciá-lo pelo comando: 

    docker start cakephp
